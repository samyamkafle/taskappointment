@extends('vendor/main')
@section('title')
<title>Todo management system |Create task|</title>
@endsection

@section('content')
<!-- start page container -->
<div class="page-container">
    <!-- start page content -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">Create Task </div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href=" /home ">Home</a>&nbsp;<i
                                class="fa fa-angle-right"></i>
                        </li>
                        <li class="active">Create task</li>
                    </ol>
                </div>
            </div>


            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card card-default">
                        <div class="card-header">
                            Create new task
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul class="list-group">
                                    @foreach ($errors->all() as $error)
                                    <li class="list-group-item">
                                        {{$error}}
                                    </li>

                                    @endforeach
                                </ul>
                            </div>

                            @endif

                            <form action="/create-todos" method="POST">
                                @csrf
                                <div class="form-group">
                                    {{-- <label for="title"> Task name: </label> --}}
                                    <input type="text" class="form-control" name="title"
                                        placeholder="Enter your task here" autofocus>
                                </div>

                                <div class="form-group">
                                    <textarea class="form-control" name="description" id="description" cols="30"
                                        rows="10" placeholder="Enter your task description here"></textarea>
                                </div>

                                <div class="form-group">
									<input class="form-control mdl-textfield__input" name="date" type="date" id="date" placeholder="Enter Date">
								</div>

                                {{-- <div class="form-group">
                                    <input type="date" class="form-control" id="date" value="{{ @$data->date }}" placeholder="Enter date" name="date">
                                </div> --}}


                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-success ">Create task</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end page content -->

</div>
<!-- end page container -->
@endsection

@section('css')
    <!-- Date Time item CSS -->
	<link rel="stylesheet" href="{{asset('assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css')}}" />
@endsection

@section('scripts')
<!-- Date Time item JS -->
    <script src="{{asset('assets/js/pages/material-select/getmdl-select.js')}}"></script>
	<script src="{{asset('assets/plugins/material-datetimepicker/moment-with-locales.min.js')}}"></script>
    <script src="{{asset('assets/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js')}}"></script>
	<script src="{{asset('assets/plugins/material-datetimepicker/datetimepicker.js')}}"></script>
@endsection


